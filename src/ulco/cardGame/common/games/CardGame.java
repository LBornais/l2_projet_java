package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.CardBoard;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.io.*;
import java.net.Socket;
import java.util.*;

/**
 * CardGame which extend BoardGame
 * BoardGame need to implement Game interface methods
 */
public class CardGame extends BoardGame {

    private List<Card> cards;
    private Integer numberOfRounds;

    /**
     * Enable constructor of CardGame
     * - Name of the game
     * - Maximum number of players of the Game
     *  - Filename of current Game
     * @param name
     * @param maxPlayers
     * @param filename
     */
    public CardGame(String name, Integer maxPlayers, String filename) {
        super(name, maxPlayers, filename);

        this.board = new CardBoard();
        this.numberOfRounds = 0;
    }

    @Override
    public void initialize(String filename) {

        this.cards = new ArrayList<>();

        // Here initialize the list of Cards
        try {
            File cardFile = new File(filename);
            Scanner myReader = new Scanner(cardFile);

            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                String[] dataValues = data.split(";");

                // get Card value
                Integer value = Integer.valueOf(dataValues[1]);
                this.cards.add(new Card(dataValues[0], value, true));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    @Override
    public Player run(Map<Player, Socket> playerSockets) {

        Player gameWinner = null;
        Object answer;

        // prepare to distribute card to each player
        Collections.shuffle(cards);

        int playerIndex = 0;

        for (Card card : cards) {

            players.get(playerIndex).addComponent(card);
            card.setPlayer(players.get(playerIndex));

            playerIndex++;

            if (playerIndex >= players.size()) {
                playerIndex = 0;
            }
        }


        // Send update of Game state for each player
        for (Map.Entry<Player, Socket> playerSocket : playerSockets.entrySet()){
            try{
                ObjectOutputStream oos1 = new ObjectOutputStream(playerSocket.getValue().getOutputStream());
                oos1.writeObject(playerSocket.getKey().getName() + " has " + playerSocket.getKey().getComponents().size() + " cards");

                ObjectOutputStream oos2 = new ObjectOutputStream(playerSocket.getValue().getOutputStream());
                oos2.writeObject(this);
            }catch (IOException e){
                e.printStackTrace();
            }
        }

        // while each player can play
        while(!end()){
                Map<Player, Card> playedCard = new HashMap<>();

            for (Map.Entry<Player, Socket> playerSocket : playerSockets.entrySet()){
                if (playerSocket.getKey().isPlaying()){
                    try{
                        ObjectOutputStream oos = new ObjectOutputStream(playerSocket.getValue().getOutputStream());
                        oos.writeObject("[" + playerSocket.getKey().getName() + "] you have to play...");
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                    for (Map.Entry<Player, Socket> state : playerSockets.entrySet()){
                        if (!state.getKey().equals(playerSocket.getKey())){
                            try{
                                ObjectOutputStream oos = new ObjectOutputStream(state.getValue().getOutputStream());
                                oos.writeObject("Waiting for " + playerSocket.getKey().getName() + " to play...");
                            }catch (IOException e){
                                e.printStackTrace();
                            }
                        }
                    }

                    try{
                        ObjectInputStream ois = new ObjectInputStream(playerSocket.getValue().getInputStream());
                        answer = ois.readObject();

                        if (answer instanceof String){
                            System.out.println(answer);
                            if (((String) answer).contains("plays")){
                                playerSocket.getKey().play(playerSocket.getValue());
                                System.out.println(playerSocket.getKey().getName() + " is now playing");
                            }
                        }

                        ObjectInputStream ois2 = new ObjectInputStream(playerSocket.getValue().getInputStream());
                        answer = ois2.readObject();

                        if (answer instanceof Card){
                            playerSocket.getKey().removeComponent((Component) answer);
                            board.addComponent((Component) answer);
                            playedCard.put(playerSocket.getKey(), (Card) answer);

                            for (Map.Entry<Player, Socket> socket : playerSockets.entrySet()){
                                ObjectOutputStream oos = new ObjectOutputStream(socket.getValue().getOutputStream());
                                oos.writeObject(playerSocket.getKey().getName() + " has played " + ((Card)answer).getName());
                            }
                        }

                    }catch (IOException e){
                        e.printStackTrace();
                    }catch(ClassNotFoundException e){
                        e.printStackTrace();
                    }
                }
            }

            for (Map.Entry<Player, Socket> playerSocket : playerSockets.entrySet()){
                try{
                    ObjectOutputStream oos = new ObjectOutputStream(playerSocket.getValue().getOutputStream());
                    oos.writeObject(board);
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            board.displayState();

            // Check which player has win
            int bestValueCard = 0;

            List<Player> possibleWinner = new ArrayList<>();
            List<Card> possibleWinnerCards = new ArrayList<>();

            for (Card card : playedCard.values()) {
                if (card.getValue() >= bestValueCard)
                    bestValueCard = card.getValue();
            }

            // check if equality
            for(Map.Entry<Player, Card> entry : playedCard.entrySet()){

                Card currentCard = entry.getValue();

                if (currentCard.getValue() >= bestValueCard) {

                    possibleWinner.add(entry.getKey());
                    possibleWinnerCards.add(currentCard);
                }
            }

            // default winner index
            int winnerIndex = 0;

            // Random choice if equality is reached
            if (possibleWinner.size() > 1){
                Random random = new Random();
                winnerIndex = random.nextInt(possibleWinner.size() - 1);
            }

            Player roundWinner = possibleWinner.get(winnerIndex);
            Card winnerCard = possibleWinnerCards.get(winnerIndex);

            for (Map.Entry<Player, Socket> playerSocket : playerSockets.entrySet()){
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(playerSocket.getValue().getOutputStream());
                    oos.writeObject("Player " + roundWinner.getName() + " won the round with " + winnerCard);
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            System.out.println("Player " + roundWinner.getName() + " won the round with " + winnerCard);

            // Update Game state
            for (Card card : playedCard.values()) {

                // remove from previous player
                roundWinner.addComponent(card);
                card.setPlayer(roundWinner);
            }

            // clear board state
            board.clear();

            // Check players State
            for(Map.Entry<Player,Socket> playerSocket : playerSockets.entrySet()){

                if(playerSocket.getKey().getScore() == 0){
                    playerSocket.getKey().canPlay(false);
                }

                if(playerSocket.getKey().getScore() == cards.size()){
                    playerSocket.getKey().canPlay(false);
                    gameWinner = playerSocket.getKey();
                }
            }

            // Display Game state
            for (Map.Entry<Player, Socket> playerSocket : playerSockets.entrySet()){
                try{
                    ObjectOutputStream oos = new ObjectOutputStream(playerSocket.getValue().getOutputStream());
                    oos.writeObject(this);
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            this.displayState();

            // shuffle player hand every n rounds
            this.numberOfRounds += 1;

            if (this.numberOfRounds % 10 == 0) {
                for (Player player : players){
                    player.shuffleHand();
                }
            }
        }

        return gameWinner;
    }

    @Override
    public boolean end() {
        // check if it's the end of the game
        endGame = true;
        for (Player player : players) {
            if (player.isPlaying()) {
                endGame = false;
            }
        }

        return endGame;
    }

    @Override
    public String toString() {
        return "CardGame{" +
                "name='" + name + '\'' +
                '}';
    }
}
